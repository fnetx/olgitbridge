# olgitbridge

Another Overleaf git bridge. This hack allows you to pull and push changes via git on an Overleaf Community Edition (CE) or Pro server.

This gitbridge uses the web API of Overleaf to down- and upsync changes. This means it does not need to run on the same server as Overleaf. In fact it is recommended to run it on another so it can directly use https port 443.

Note that contrary to the cloud version this bridge does not use realtime change operations, any files changed by git will result in a "this file has been changed externally" interruption in the online editor.

## Getting started

Start installing (likely on another server than the Overleaf server)

```
git clone https://gitlab.com/axkibe/olgitbridge.git
cd olgitbridge
npm install
```

edit config.js in your favorite editor.

If the bridge is running on port 80 and or 443 using authbind may be a good idea.

Start the server with (or without authbind)

```
authbind node src/server.js
```

Wrap it in your auto(re)starter/service of your choice.

## Dependencies

In development node.js v16.11.0 was used.
